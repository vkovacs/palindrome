package hu.crs.palindrome

class Palindrome {

    fun palindrome(text: String): Boolean {
        return text == reverse(text)
    }

    fun reverse(text: String): String {
        return text.reversed()
    }
}