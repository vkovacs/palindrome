package hu.crs.palindrome

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PalindromeTest {
    @Test
    fun `reverse success` () {
        Assertions.assertEquals("abc", Palindrome().reverse("cba"))
    }

    @Test
    internal fun `text is palindrome`() {
        Assertions.assertTrue(Palindrome().palindrome("abba"))
    }

    @Test
    internal fun `text is not palindrome`() {
        Assertions.assertFalse(Palindrome().palindrome("abc"))
    }
}