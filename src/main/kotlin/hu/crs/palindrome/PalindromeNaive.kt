package hu.crs.palindrome

class PalindromeNaive {

    fun palindrome(text: String): Boolean {
        return text == reverse(text)
    }

    fun reverse(text: String): String {
        var reversedText = ""
        for (i in (text.length - 1) downTo 0) {
            reversedText = reversedText + text.get(i);
        }
        return reversedText
    }
}