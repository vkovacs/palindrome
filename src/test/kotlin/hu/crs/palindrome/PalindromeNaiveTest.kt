package hu.crs.palindrome

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test

class PalindromeNaiveTest {
    @Test
    fun `reverse success` () {
        Assertions.assertEquals("abc", PalindromeNaive().reverse("cba"))
    }
    @Test
    internal fun `text is palindrome`() {
        Assertions.assertTrue(PalindromeNaive().palindrome("abba"))
    }

    @Test
    internal fun `text is not palindrome`() {
        Assertions.assertFalse(PalindromeNaive().palindrome("abc"))
    }
}